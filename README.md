# Nodejs 12 #

Neste exemplo, utilizamos um container com versão linux conhecida por Buster Slim. 
Esta versão contém um tamanho reduzido e possui apenas os requisitos mínimos para execução de scripts em nodejs.
Note que a pasta /src contém um exemplo de código em nodejs + express (hello world), que é executado no final.

### Comandos ###
docker-compose up

http://localhost:3000