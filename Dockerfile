FROM node:12-buster-slim

WORKDIR /app

COPY ./src .

EXPOSE 3000

CMD [ "npm", "start" ]
